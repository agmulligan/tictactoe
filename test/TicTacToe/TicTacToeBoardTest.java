/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TicTacToe;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ashleymulligan
 */
public class TicTacToeBoardTest {
    
    public TicTacToeBoardTest() {
    }

    /**
     * Test of reset method, of class TicTacToeBoard.
     */
    @Test
    public void testReset() {
        System.out.println("reset");
        TicTacToeBoard instance = new TicTacToeBoard();
        instance.reset();
    }

    /**
     * Test of getSquare method, of class TicTacToeBoard.
     */
    @Test
    public void testGetSquare() {
        System.out.println("getSquare");
        String square = "";
        TicTacToeBoard instance = new TicTacToeBoard();
        int expResult = 0;
        int result = instance.getSquare(square);
        System.out.println(result);
        assertEquals(expResult, result);
    }

    /**
     * Test of playAt method, of class TicTacToeBoard.
     */
    @Test
    public void testPlayAt() {
        System.out.println("playAt");
        String square = "";
        int player = 1;
        TicTacToeBoard instance = new TicTacToeBoard();
        instance.playAt(square, player);
    }

    /**
     * Test of isGameOver method, of class TicTacToeBoard.
     */
    @Test
    public void testIsGameOver() {
        System.out.println("isGameOver");
        TicTacToeBoard instance = new TicTacToeBoard();
        int expResult = 0;
        int result = instance.isGameOver();
        assertEquals(expResult, result);
    }

    /**
     * Test of canPlayerWin method, of class TicTacToeBoard.
     */
    @Test
    public void testCanPlayerWin() {
        System.out.println("canPlayerWin");
        int player = 1;
        TicTacToeBoard instance = new TicTacToeBoard();
        String expResult = "";
        String result = instance.canPlayerWin(player);
        assertEquals(expResult, result);
    }

    /**
     * Test of getNextMove method, of class TicTacToeBoard.
     */
    @Test
    public void testGetNextMove() {
        System.out.println("getNextMove");
        TicTacToeBoard instance = new TicTacToeBoard();
        String expResult = "";
        String result = instance.getNextMove();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class TicTacToeBoard.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        TicTacToeBoard instance = new TicTacToeBoard();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
