# README #

This code was a tutorial from: http://www.dummies.com/store/product/Java-All-in-One-For-Dummies-4th-Edition.productCd-1118408039,descCd-DOWNLOAD.html

I had spent a lot of time trying to create a game from scratch so I thought if I found a helper tutorial I could focus more on the Unit Tests, which is the true goal of this exercise. 

These unit tests were somewhat auto created from the java files. I didn't even mean to do that, actually. I didn't know that was possible and hit create test and it happened magically. Once they were auto generate, there were two that were failing and the rest are passing, with 2 having warnings.

**testGetSquare** and **testPlayAt** - both of these were failing upon the auto generated test cases because the square object was being set to a null string. 

I altered those two null values to C3, which is a button value and the tests ran as intended.

**toString** was also modified to pass because it expected the board String, which is:
   |   |  
-----------
   |   |  
-----------
   |   |  

**testGetNextMove** - It defaults to the center circle as the best move always if it's open: 
        if (board[4] == 2) {
            return "B2";
        }

then it goes for the first open corner.

Maybe I'm not doing this how it should be done but I still don't quite understand how this is helping with specific bugs. I see how it helps in that if you change something over here you can test to make sure everything else is working as expected but for a specific unit, I don't find it as useful.